package com.namigtahmazli.shortcuttestapp

import android.app.Application
import com.namigtahmazli.shortcuttestapp.di.AppComponent
import com.namigtahmazli.shortcuttestapp.di.DaggerAppComponent

class App : Application() {
    val component: AppComponent by lazy(LazyThreadSafetyMode.NONE) {
        DaggerAppComponent.factory().create(this)
    }

    override fun onCreate() {
        component.inject(this)
        super.onCreate()
    }
}