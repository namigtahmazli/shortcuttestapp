package com.namigtahmazli.shortcuttestapp

import android.util.Log
import com.namigtahmazli.shortcuttestapp.core.logger.Logger
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

internal class LoggerImpl @Inject constructor(): Logger {
    override fun logError(tag: String, context: CoroutineContext, error: Throwable) {
        Log.d(tag, "logError: ${error.stackTrace}, $context")
    }
}