package com.namigtahmazli.shortcuttestapp.di

import com.namigtahmazli.shortcuttestapp.ui.home.di.HomeSubcomponent
import dagger.Module

@Module(subcomponents = [HomeSubcomponent::class])
abstract class ActivitiesModule