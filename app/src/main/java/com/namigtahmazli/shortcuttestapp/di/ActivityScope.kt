package com.namigtahmazli.shortcuttestapp.di

import javax.inject.Scope

@Scope
annotation class ActivityScope