package com.namigtahmazli.shortcuttestapp.di

import com.namigtahmazli.shortcuttestapp.App
import com.namigtahmazli.shortcuttestapp.core.di.AppScope
import com.namigtahmazli.shortcuttestapp.core.di.CoreModule
import com.namigtahmazli.shortcuttestapp.fileManager.di.FileManagerModule
import com.namigtahmazli.shortcuttestapp.jsonParser.di.JsonParserModule
import com.namigtahmazli.shortcuttestapp.localSource.LocalSourceModule
import com.namigtahmazli.shortcuttestapp.remoteSource.di.RemoteSourceModule
import com.namigtahmazli.shortcuttestapp.ui.home.di.HomeSubcomponent
import dagger.BindsInstance
import dagger.Component

@AppScope
@Component(
    modules = [
        CoreModule::class,
        RemoteSourceModule::class,
        JsonParserModule::class,
        ActivitiesModule::class,
        AppModule::class,
        LocalSourceModule::class,
        FileManagerModule::class
    ]
)
interface AppComponent {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance app: App): AppComponent
    }

    val homeSubcomponentFactory: HomeSubcomponent.Factory

    fun inject(app: App)
}

