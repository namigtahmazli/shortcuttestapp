package com.namigtahmazli.shortcuttestapp.di

import android.content.Context
import com.namigtahmazli.shortcuttestapp.App
import com.namigtahmazli.shortcuttestapp.LoggerImpl
import com.namigtahmazli.shortcuttestapp.core.di.AppScope
import com.namigtahmazli.shortcuttestapp.core.logger.Logger
import dagger.Binds
import dagger.Module
import dagger.Provides

@Module
abstract class AppModule {
    @Binds
    @AppScope
    internal abstract fun bindLogger(impl: LoggerImpl): Logger

    companion object {
        @Provides
        @AppScope
        fun provideContext(app: App): Context = app.applicationContext
    }
}