package com.namigtahmazli.shortcuttestapp.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import javax.inject.Inject
import javax.inject.Provider

class ViewModelProviderFactory @Inject constructor(
    private val viewModels: Map<Class<out ViewModel>, @JvmSuppressWildcards Provider<ViewModel>>
) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        return viewModels.entries
            .firstOrNull { it.key.isAssignableFrom(modelClass) }
            ?.value?.get() as? T
            ?: throw IllegalStateException("Could not find ViewModel for $modelClass")
    }
}