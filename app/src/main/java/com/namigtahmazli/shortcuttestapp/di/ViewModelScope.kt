package com.namigtahmazli.shortcuttestapp.di

import androidx.lifecycle.ViewModel
import dagger.MapKey
import kotlin.reflect.KClass

@MapKey
@Target(AnnotationTarget.FUNCTION)
annotation class ViewModelScope(val type: KClass<out ViewModel>)
