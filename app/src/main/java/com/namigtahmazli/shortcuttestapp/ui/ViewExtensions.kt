package com.namigtahmazli.shortcuttestapp.ui

import android.view.View
import android.view.ViewGroup
import androidx.core.view.*

data class Margins(
    val left: Int,
    val top: Int,
    val right: Int,
    val bottom: Int
)

internal inline fun <reified LP : ViewGroup.LayoutParams> View.applyWindowInsets(
    crossinline block: LP.(View, Margins) -> Unit
) {
    val margin = Margins(
        marginLeft,
        marginTop,
        marginRight,
        marginBottom
    )

    ViewCompat.setOnApplyWindowInsetsListener(this) { view, insets ->
        val inset = insets.getInsets(WindowInsetsCompat.Type.systemBars())
        val newMargin = margin.run {
            copy(
                top = top + inset.top,
                bottom = bottom + inset.bottom
            )
        }
        updateLayoutParams<LP> {
            block(view, newMargin)
        }
        insets
    }
}