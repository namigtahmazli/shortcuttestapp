package com.namigtahmazli.shortcuttestapp.ui.home

import android.annotation.SuppressLint
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.view.doOnLayout
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.Glide
import com.namigtahmazli.shortcuttestapp.App
import com.namigtahmazli.shortcuttestapp.R
import com.namigtahmazli.shortcuttestapp.core.extensions.exhaustive
import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsStore
import com.namigtahmazli.shortcuttestapp.databinding.ActivityHomeBinding
import com.namigtahmazli.shortcuttestapp.di.ViewModelProviderFactory
import com.namigtahmazli.shortcuttestapp.ui.applyWindowInsets
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHomeBinding

    @Inject
    lateinit var factory: ViewModelProviderFactory

    private val viewModel: HomeViewModel by viewModels { factory }

    private lateinit var zoomTouchListener: ZoomTouchListener

    override fun onCreate(savedInstanceState: Bundle?) {
        inject()
        super.onCreate(savedInstanceState)
        createView()
        applyWindowInsets()
        observeState()
        observeEvents()
        setUpListeners()
    }

    private fun inject() {
        (application as App).component.homeSubcomponentFactory
            .create(this)
            .inject(this)
    }

    private fun createView() {
        binding = ActivityHomeBinding.inflate(layoutInflater)
        setContentView(binding.root)
        zoomTouchListener = ZoomTouchListener(this)
    }

    private fun applyWindowInsets() {
        binding.run {
            btnPrev.applyWindowInsets<ConstraintLayout.LayoutParams> { _, margin ->
                topMargin = margin.top
            }

            btnFavorite.applyWindowInsets<ConstraintLayout.LayoutParams> { _, margin ->
                bottomMargin = margin.bottom
            }
        }
    }

    private fun observeState() {
        binding.run {
            lifecycleScope.launch {
                viewModel.states.collect { state ->
                    with(state) {
                        progressBar.isVisible = isLoading
                        if (isLoading)
                            disableButtons()
                        else
                            enableButtons()
                        if (isTooltipShown)
                            showTooltip()
                        else
                            hideTooltip()
                        currentShownComic?.let(::displayComic)
                    }
                }
            }
        }
    }

    private fun showTooltip() {
        binding.run {
            tooltip.isVisible = true
            tooltip.doOnLayout {
                val centerX = btnInfo.x - tooltip.x + btnInfo.measuredWidth / 2
                tooltip.setTriangleCenter(centerX)
            }
        }
    }

    private fun hideTooltip() {
        binding.tooltip.isVisible = false
    }

    private fun displayComic(comic: Comic) {
        comic.run {
            showComicImage(image)
            displayComicTranscript(details.run { transcript ?: alt })
            updateFavoriteButtonState(comic.isFavorite)
        }
    }

    private fun showComicImage(url: String) {
        binding.run {
            Glide.with(image)
                .load(url)
                .into(image)
            zoomTouchListener.animateBack()
        }
    }

    private fun displayComicTranscript(transcript: String) {
        binding.tooltip.text = transcript
    }

    private fun updateFavoriteButtonState(isFavorite: Boolean) {
        binding.btnFavorite.setImageResource(
            if (isFavorite) R.drawable.ic_favorite else R.drawable.ic_not_favorite
        )
    }

    private fun observeEvents() {
        lifecycleScope.launch {
            viewModel.events.collect {
                when(it) {
                    is ComicsStore.Event.Share -> shareComic(it.uri)
                }.exhaustive
            }
        }
    }

    private fun shareComic(uri: String) {
        val intent = Intent().apply {
            action = Intent.ACTION_SEND
            putExtra(Intent.EXTRA_STREAM, Uri.parse(uri))
            type = "image/jpeg"
        }
        startActivity(Intent.createChooser(intent, "Share Comic"))
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setUpListeners() {
        binding.btnPrev.setOnClickListener(::onViewClicked)
        binding.btnNext.setOnClickListener(::onViewClicked)
        binding.btnInfo.setOnClickListener(::onViewClicked)
        binding.btnFavorite.setOnClickListener(::onViewClicked)
        binding.btnShare.setOnClickListener(::onViewClicked)
        binding.image.setOnTouchListener(zoomTouchListener)
    }

    private fun onViewClicked(view: View) {
        binding.run {
            when (view) {
                btnPrev -> viewModel.loadPreviousComic()
                btnNext -> viewModel.loadNextComic()
                btnInfo -> viewModel.toggleTooltip()
                btnFavorite -> viewModel.toggleFavorite()
                btnShare -> viewModel.shareComic()
            }
        }
    }

    private fun disableButtons() {
        getButtons { it.isEnabled = false }
    }

    private fun enableButtons() {
        getButtons { it.isEnabled = true }
    }

    private inline fun getButtons(f: (View) -> Unit) {
        binding.run { listOf(btnFavorite, btnPrev, btnInfo, btnNext, btnShare) }.onEach(f)
    }
}