package com.namigtahmazli.shortcuttestapp.ui.home

import androidx.lifecycle.ViewModel
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsStoreController
import com.namigtahmazli.shortcuttestapp.di.ActivityScope
import javax.inject.Inject

@ActivityScope
class HomeViewModel @Inject constructor(
    private val factory: ComicsStoreController.Factory
) : ViewModel(), ComicsStoreController by factory.create() {
    override fun onCleared() {
        dispose()
        super.onCleared()
    }
}