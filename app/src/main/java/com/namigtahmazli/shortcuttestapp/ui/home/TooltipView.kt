package com.namigtahmazli.shortcuttestapp.ui.home

import android.content.Context
import android.content.res.Resources
import android.graphics.*
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

val Int.dp get() = Resources.getSystem().displayMetrics.density * this
val Float.i get() = toInt()
val Int.f get() = toFloat()

class TooltipView @JvmOverloads constructor(
    context: Context,
    attributeSet: AttributeSet? = null,
    defaultStyleAttr: Int = 0
) : AppCompatTextView(context, attributeSet, defaultStyleAttr) {
    private val padding = 16.dp.i
    private val triangleHeight = 16.dp
    private val bounds = Rect()
    private val path = Path()

    private val paint: Paint = Paint().apply {
        isAntiAlias = true
        style = Paint.Style.FILL
        color = Color.GRAY
    }

    private var triangleCenterX: Float = 0f

    init {
        setPadding(padding, padding + triangleHeight.i, padding, padding)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        bounds.set(left, top, right, bottom)
        triangleCenterX = bounds.centerX().f
    }

    override fun draw(canvas: Canvas?) {
        canvas?.run {
            path.reset()
            path.moveTo(0f, triangleHeight)
            path.lineTo(triangleCenterX - triangleHeight / 2, triangleHeight)
            path.lineTo(triangleCenterX, 0f)
            path.lineTo(triangleCenterX + triangleHeight / 2, triangleHeight)
            path.lineTo(bounds.right.f, triangleHeight)
            path.lineTo(bounds.right.f, bounds.bottom.f)
            path.lineTo(0f, bounds.bottom.f)
            path.lineTo(0f, triangleHeight)
            path.close()

            drawPath(path, paint)
        }
        super.draw(canvas)
    }

    fun setTriangleCenter(center: Float) {
        triangleCenterX = center
        invalidate()
    }
}