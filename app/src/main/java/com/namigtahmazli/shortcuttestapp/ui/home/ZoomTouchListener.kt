package com.namigtahmazli.shortcuttestapp.ui.home

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.PointF
import android.util.SparseArray
import android.view.GestureDetector
import android.view.MotionEvent
import android.view.ScaleGestureDetector
import android.view.View
import androidx.core.util.set
import kotlin.math.max
import kotlin.math.min

class ZoomTouchListener(context: Context) : View.OnTouchListener,
    ScaleGestureDetector.OnScaleGestureListener {

    private var scaleFactor: Float = 1f
    private var span: Float = 0f
    private var view: View? = null
    private val points = SparseArray<PointF>()

    private val scaleDetector: ScaleGestureDetector = ScaleGestureDetector(context, this)
    private val gestureDetector: GestureDetector = GestureDetector(
        context, object : GestureDetector.SimpleOnGestureListener() {
            override fun onDoubleTap(e: MotionEvent?): Boolean {
                animateBack()
                return true
            }
        })

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        if (v == null || event == null) return false
        view = v
        rememberDownPoints(event)
        if (move(v, event)) return true
        if (gestureDetector.onTouchEvent(event)) return true
        if (scaleDetector.onTouchEvent(event)) return true
        return false
    }

    override fun onScale(detector: ScaleGestureDetector): Boolean {
        val movement = detector.currentSpan - span
        view?.let { view ->
            scaleFactor += movement / min(view.width, view.height)
            scaleFactor = max(.7f, min(2f, scaleFactor))
            view.scaleX = scaleFactor
            view.scaleY = scaleFactor
        }
        return true
    }

    override fun onScaleBegin(detector: ScaleGestureDetector): Boolean {
        span = detector.currentSpan
        return true
    }

    override fun onScaleEnd(detector: ScaleGestureDetector?) {
        span = 0f
    }

    private fun rememberDownPoints(event: MotionEvent) {
        when (event.actionMasked) {
            MotionEvent.ACTION_DOWN -> rememberPoint(event)
            MotionEvent.ACTION_POINTER_DOWN -> rememberPoint(event)
            MotionEvent.ACTION_UP -> clearPoint(event)
            MotionEvent.ACTION_POINTER_UP -> clearPoint(event)
        }
    }

    private fun rememberPoint(event: MotionEvent) {
        points[getPointerId(event)] = PointF(
            event.getX(event.actionIndex),
            event.getY(event.actionIndex)
        )
    }

    private fun getPointerId(event: MotionEvent): Int {
        return event.getPointerId(event.actionIndex)
    }

    private fun clearPoint(event: MotionEvent) {
        points.remove(getPointerId(event))
        if (points.size() == 1) {
            points.clear()
        }
    }

    private fun move(view: View, event: MotionEvent): Boolean {
        if (event.pointerCount > 1) return false
        return when (event.actionMasked) {
            MotionEvent.ACTION_MOVE -> {
                if (points.size() < event.pointerCount) {
                    rememberPoint(event)
                }
                val currentPoint = getPoint(event)
                val initialPoint = getInitialPoint(event)
                view.x += currentPoint.x - initialPoint.x
                view.y += currentPoint.y - initialPoint.y
                true
            }
            else -> false
        }
    }

    private fun getPoint(event: MotionEvent): PointF {
        return PointF(
            event.getX(event.actionIndex),
            event.getY(event.actionIndex)
        )
    }

    private fun getInitialPoint(event: MotionEvent): PointF {
        val pointerId = getPointerId(event)
        return points[pointerId] ?: getPoint(event)
    }

    fun animateBack() {
        view?.animate()
            ?.x(0f)
            ?.y(0f)
            ?.scaleX(1f)
            ?.scaleY(1f)
            ?.setDuration(300)
            ?.withEndAction { scaleFactor = 1f }
            ?.start()
    }
}