package com.namigtahmazli.shortcuttestapp.ui.home.di

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.namigtahmazli.shortcuttestapp.di.ViewModelScope
import com.namigtahmazli.shortcuttestapp.ui.home.HomeViewModel
import dagger.Binds
import dagger.Module
import dagger.Provides
import dagger.multibindings.IntoMap
import kotlin.coroutines.CoroutineContext

@Module
abstract class HomeModule {
    @Binds
    @ViewModelScope(HomeViewModel::class)
    @IntoMap
    abstract fun bind(viewModel: HomeViewModel): ViewModel

    companion object {
        @Provides
        fun provideParentContext(viewModel: HomeViewModel): CoroutineContext =
            viewModel.viewModelScope.coroutineContext
    }
}