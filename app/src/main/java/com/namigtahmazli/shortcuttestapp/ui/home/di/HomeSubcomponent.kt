package com.namigtahmazli.shortcuttestapp.ui.home.di

import com.namigtahmazli.shortcuttestapp.di.ActivityScope
import com.namigtahmazli.shortcuttestapp.ui.home.HomeActivity
import dagger.BindsInstance
import dagger.Subcomponent

@ActivityScope
@Subcomponent(modules = [HomeModule::class])
interface HomeSubcomponent {
    @Subcomponent.Factory
    interface Factory {
        fun create(@BindsInstance activity: HomeActivity): HomeSubcomponent
    }

    fun inject(activity: HomeActivity)
}