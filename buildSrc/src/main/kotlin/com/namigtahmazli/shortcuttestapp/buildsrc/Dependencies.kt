package com.namigtahmazli.shortcuttestapp.buildsrc

object Dependencies {
    object Kotlin {
        const val version = "1.5.20"
        const val plugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$version"
    }

    object Coroutines {
        private const val version = "1.5.1"
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
    }

    object Dagger {
        private const val version = "2.38.1"
        const val core = "com.google.dagger:dagger:$version"
        const val compiler = "com.google.dagger:dagger-compiler:$version"
    }

    object Ktor {
        private const val version = "1.6.3"
        const val core = "io.ktor:ktor-client-core:$version"
        const val androidClient = "io.ktor:ktor-client-android:$version"
        const val logger = "io.ktor:ktor-client-logging:$version"
    }

    object KotlinxSerialisation {
        private const val version = "1.2.2"
        const val plugin = "org.jetbrains.kotlin:kotlin-serialization:${Kotlin.version}"
        const val json = "org.jetbrains.kotlinx:kotlinx-serialization-json:$version"
    }

    object Testing {
        const val jUnit = "junit:junit:4.13.2"
        const val mockK = "io.mockk:mockk:1.12.0"
        const val turbine = "app.cash.turbine:turbine:0.6.1"
    }

    object AndroidX {
        const val core = "androidx.core:core-ktx:1.6.0"
        const val appcompat = "androidx.appcompat:appcompat:1.3.1"
        const val activity = "androidx.activity:activity-ktx:1.3.1"
        const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.1.0"
    }

    object Material {
        const val core = "com.google.android.material:material:1.4.0"
    }

    object Other {
        const val glide = "com.github.bumptech.glide:glide:4.12.0"
    }

    object SqlDelight {
        private const val version = "1.5.0"
        const val plugin = "com.squareup.sqldelight:gradle-plugin:$version"
        const val androidDriver = "com.squareup.sqldelight:android-driver:$version"
    }
}