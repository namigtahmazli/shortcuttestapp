package com.namigtahmazli.shortcuttestapp.core.di

import com.namigtahmazli.shortcuttestapp.core.interactors.di.Interactors
import com.namigtahmazli.shortcuttestapp.core.mvi.*
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsExecutor
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsExecutorDependency
import dagger.Binds
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers

@Module(
    includes = [
        Interactors::class
    ]
)
abstract class CoreModule {
    @Binds
    internal abstract fun bindDependency(
        impl: ComicsExecutorDependency
    ): ComicsExecutor.Dependency

    @Binds
    internal abstract fun bindControllerFactory(
        impl: ComicsStoreControllerFactory
    ): ComicsStoreController.Factory

    companion object {
        @Provides
        fun provideDispatcher(): CoroutineDispatcher = Dispatchers.IO
    }
}