package com.namigtahmazli.shortcuttestapp.core.dto

import com.namigtahmazli.shortcuttestapp.core.models.Comic

interface ComicDto {
    val month: String
    val num: Int
    val year: String
    val safeTitle: String
    val transcript: String
    val alt: String
    val img: String
    val day: String

    fun toModel(isFavorite: Boolean): Comic = Comic(
        title = safeTitle,
        image = img,
        number = num,
        date = Comic.Date(
            year = year,
            month = month,
            day = day
        ),
        details = Comic.Details(
            transcript = transcript.takeIf { it.isNotBlank() },
            alt = alt
        ),
        isFavorite = isFavorite
    )

    companion object
}