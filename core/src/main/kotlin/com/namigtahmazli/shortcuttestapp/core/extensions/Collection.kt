package com.namigtahmazli.shortcuttestapp.core.extensions

inline fun <T> Set<T>.replace(with: T, where: (T) -> Boolean) : Set<T> {
    return (filterNot(where) + with).toSet()
}