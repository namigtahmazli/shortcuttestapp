package com.namigtahmazli.shortcuttestapp.core.extensions

fun String.append(string: String): String {
    return this + string
}