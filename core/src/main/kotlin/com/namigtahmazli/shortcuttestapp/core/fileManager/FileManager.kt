package com.namigtahmazli.shortcuttestapp.core.fileManager

import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import java.io.File

interface FileManager {
    fun getFile(name: String): Result<File, AppError>
    fun createTemporaryFile(name: String): Result<File, AppError>
    fun removeFileWith(name: String): Result<Unit, AppError>
    fun createUriForFile(name: String): Result<String, AppError>
}