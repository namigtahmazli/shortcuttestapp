package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.localSource.LocalSource
import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.result.*
import javax.inject.Inject

interface ChangeFavoriteStatusOfComic {
    suspend operator fun invoke(
        comic: Comic,
        switchThread: Boolean = true
    ): Result<Comic, AppError>
}

internal class ChangeFavoriteStatusOfComicImpl @Inject constructor(
    private val threadDispatcher: ThreadDispatcher,
    private val isComicFavorite: IsComicFavorite,
    private val localSource: LocalSource
) : ChangeFavoriteStatusOfComic {
    override suspend fun invoke(
        comic: Comic,
        switchThread: Boolean
    ): Result<Comic, AppError> {
        return threadDispatcher.dispatch(switchThread) {
            isComicFavorite(comic.number, false)
                .flatMap { isFavorite ->
                    (if (isFavorite) localSource.removeComic(comic.number)
                    else localSource.saveComic(comic))
                        .map { comic.copy(isFavorite = !isFavorite) }
                }
        }
    }
}