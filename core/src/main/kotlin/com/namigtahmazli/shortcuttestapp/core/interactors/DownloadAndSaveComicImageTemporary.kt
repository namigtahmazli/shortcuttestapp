package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.extensions.append
import com.namigtahmazli.shortcuttestapp.core.fileManager.FileManager
import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.remoteSource.RemoteSource
import com.namigtahmazli.shortcuttestapp.core.result.*
import java.io.File
import javax.inject.Inject

interface DownloadAndSaveComicImageTemporary {
    suspend operator fun invoke(
        comic: Comic,
        switchThread: Boolean = true,
    ): Result<File, AppError>
}

internal class DownloadAndSaveComicImageTemporaryImpl @Inject constructor(
    private val threadDispatcher: ThreadDispatcher,
    private val fileManager: FileManager,
    private val remoteSource: RemoteSource
): DownloadAndSaveComicImageTemporary {
    override suspend fun invoke(
        comic: Comic,
        switchThread: Boolean
    ): Result<File, AppError> {
        return threadDispatcher.dispatch(switchThread) {
            fileManager.createTemporaryFile(comic.number.toString().append(".jpeg"))
                .flatMap { file ->
                    remoteSource.downloadComicImage(comic.image, file)
                        .toResult()
                        .map { file }
                        .catchAndFallback { fileManager.removeFileWith(file.name) }
                }
        }
    }
}