package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.parsers.ComicsJsonParser
import com.namigtahmazli.shortcuttestapp.core.dto.ComicDto
import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.remoteSource.RemoteSource
import com.namigtahmazli.shortcuttestapp.core.remoteSource.Response
import com.namigtahmazli.shortcuttestapp.core.result.*
import javax.inject.Inject

interface FetchComicDetails {
    suspend operator fun invoke(
        number: Int,
        switchThread: Boolean = true
    ): Result<Comic, AppError>
}

internal class FetchComicDetailsImpl @Inject constructor(
    private val remoteSource: RemoteSource,
    private val threadDispatcher: ThreadDispatcher,
    private val comicsJsonParser: ComicsJsonParser,
    private val isComicFavorite: IsComicFavorite
) : FetchComicDetails {
    override suspend fun invoke(
        number: Int,
        switchThread: Boolean
    ): Result<Comic, AppError> {
        return threadDispatcher.dispatch(switchThread) {
            remoteSource.fetchComicDetails(comicNumber = number)
                .toResult()
                .map(Response.Succeed::json)
                .flatMap(comicsJsonParser::fromJson)
                .zipWith { isComicFavorite(it.num, false) }
                .map { (dto, isFavorite) -> dto.toModel(isFavorite) }
        }
    }
}