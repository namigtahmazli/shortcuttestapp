package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.parsers.ComicsJsonParser
import com.namigtahmazli.shortcuttestapp.core.remoteSource.RemoteSource
import com.namigtahmazli.shortcuttestapp.core.remoteSource.Response
import com.namigtahmazli.shortcuttestapp.core.result.*
import javax.inject.Inject

interface FetchCurrentComic {
    suspend operator fun invoke(switchThread: Boolean = true): Result<Comic, AppError>
}

internal class FetchCurrentComicImpl @Inject constructor(
    private val remoteSource: RemoteSource,
    private val comicsJsonParser: ComicsJsonParser,
    private val threadDispatcher: ThreadDispatcher,
    private val getComicDetails: GetComicDetails
) : FetchCurrentComic {
    override suspend fun invoke(switchThread: Boolean): Result<Comic, AppError> {
        return threadDispatcher.dispatch(switchThread) {
            remoteSource
                .fetchCurrentComic()
                .toResult()
                .map(Response.Succeed::json)
                .flatMap(comicsJsonParser::fromJson)
                .flatMap { getComicDetails(it.num, false) }
        }
    }
}