package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import com.namigtahmazli.shortcuttestapp.core.result.catchFlatMap
import javax.inject.Inject

interface GetComicDetails {
    suspend operator fun invoke(number: Int, switchThread: Boolean = true): Result<Comic, AppError>
}

internal class GetComicDetailsImpl @Inject constructor(
    private val loadComicFromLocal: LoadComicFromLocal,
    private val fetchComicDetails: FetchComicDetails,
    private val threadDispatcher: ThreadDispatcher
): GetComicDetails {
    override suspend fun invoke(
        number: Int,
        switchThread: Boolean
    ): Result<Comic, AppError> {
        return threadDispatcher.dispatch(switchThread) {
            loadComicFromLocal(number, false)
                .catchFlatMap { fetchComicDetails(number, false) }
        }
    }

}