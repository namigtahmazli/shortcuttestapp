package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.extensions.append
import com.namigtahmazli.shortcuttestapp.core.fileManager.FileManager
import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import com.namigtahmazli.shortcuttestapp.core.result.catchFlatMap
import com.namigtahmazli.shortcuttestapp.core.result.flatMap
import javax.inject.Inject

interface GetComicImageUri {
    suspend operator fun invoke(
        comic: Comic,
        switchThread: Boolean = true
    ): Result<String, AppError>
}

internal class GetComicImageUriImpl @Inject constructor(
    private val fileManager: FileManager,
    private val threadDispatcher: ThreadDispatcher,
    private val downloadAndSaveComicImageTemporary: DownloadAndSaveComicImageTemporary
) : GetComicImageUri {
    override suspend fun invoke(
        comic: Comic, switchThread: Boolean
    ): Result<String, AppError> {
        return threadDispatcher.dispatch(switchThread) {
            fileManager.getFile(comic.number.toString().append(".jpeg"))
                .catchFlatMap { downloadAndSaveComicImageTemporary(comic, false) }
                .flatMap { fileManager.createUriForFile(it.name) }
        }
    }
}