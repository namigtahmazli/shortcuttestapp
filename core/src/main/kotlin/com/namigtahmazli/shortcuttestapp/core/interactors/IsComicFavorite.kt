package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.localSource.LocalSource
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import com.namigtahmazli.shortcuttestapp.core.result.catchMap
import com.namigtahmazli.shortcuttestapp.core.result.map
import javax.inject.Inject

interface IsComicFavorite {
    suspend operator fun invoke(number: Int, switchThread: Boolean = true)
        : Result<Boolean, AppError>
}

internal class IsComicFavoriteImpl @Inject constructor(
    private val localSource: LocalSource,
    private val threadDispatcher: ThreadDispatcher
) : IsComicFavorite {
    override suspend fun invoke(
        number: Int,
        switchThread: Boolean
    ): Result<Boolean, AppError> {
        return threadDispatcher.dispatch(switchThread) {
            localSource.retrieveComic(number)
                .map { true }
                .catchMap { false }
        }
    }
}