package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.localSource.LocalSource
import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import com.namigtahmazli.shortcuttestapp.core.result.map
import javax.inject.Inject

interface LoadComicFromLocal {
    suspend operator fun invoke(
        number: Int,
        switchThread: Boolean = true
    ): Result<Comic, AppError>
}

internal class LoadComicFromLocalImpl @Inject constructor(
    private val localSource: LocalSource,
    private val threadDispatcher: ThreadDispatcher
) : LoadComicFromLocal {
    override suspend fun invoke(
        number: Int,
        switchThread: Boolean
    ): Result<Comic, AppError> {
        return threadDispatcher.dispatch(switchThread) {
            localSource.retrieveComic(number)
                .map { it.toModel(true) }
        }
    }
}