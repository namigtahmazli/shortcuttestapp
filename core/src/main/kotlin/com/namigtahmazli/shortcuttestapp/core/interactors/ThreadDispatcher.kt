package com.namigtahmazli.shortcuttestapp.core.interactors

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class ThreadDispatcher @Inject constructor(
    private val backgroundDispatcher: CoroutineDispatcher
) {
    suspend fun <T : Any?> dispatch(
        newThread: Boolean,
        job: suspend CoroutineScope.() -> T
    ): T =
        if (newThread)
            withContext(backgroundDispatcher, block = job)
        else
            withContext(Dispatchers.Unconfined, block = job)
}