package com.namigtahmazli.shortcuttestapp.core.interactors.di

import com.namigtahmazli.shortcuttestapp.core.interactors.*
import com.namigtahmazli.shortcuttestapp.core.interactors.ChangeFavoriteStatusOfComicImpl
import com.namigtahmazli.shortcuttestapp.core.interactors.FetchComicDetailsImpl
import com.namigtahmazli.shortcuttestapp.core.interactors.FetchCurrentComicImpl
import dagger.Binds
import dagger.Module

@Module
internal abstract class Interactors {
    @Binds
    abstract fun bindFetchCurrentComic(impl: FetchCurrentComicImpl): FetchCurrentComic

    @Binds
    abstract fun bindFetchComicDetails(impl: FetchComicDetailsImpl): FetchComicDetails

    @Binds
    abstract fun bindChangeFavoriteStatusOfComic(
        impl: ChangeFavoriteStatusOfComicImpl
    ): ChangeFavoriteStatusOfComic

    @Binds
    abstract fun bindGetComicDetails(
        impl: GetComicDetailsImpl
    ): GetComicDetails

    @Binds
    abstract fun bindIsFavoriteComic(
        impl: IsComicFavoriteImpl
    ): IsComicFavorite

    @Binds
    abstract fun bindLoadComicFromLocal(
        impl: LoadComicFromLocalImpl
    ): LoadComicFromLocal

    @Binds
    abstract fun bindGetComicImageUri(
        impl: GetComicImageUriImpl
    ): GetComicImageUri

    @Binds
    abstract fun bindDownloadAndSaveComicImageTemporary(
        impl: DownloadAndSaveComicImageTemporaryImpl
    ): DownloadAndSaveComicImageTemporary
}