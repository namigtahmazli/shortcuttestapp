package com.namigtahmazli.shortcuttestapp.core.localSource

import com.namigtahmazli.shortcuttestapp.core.dto.ComicDto
import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result

interface LocalSource {
    suspend fun saveComic(comic: Comic): Result<Unit, AppError>
    suspend fun retrieveComic(number: Int): Result<ComicDto, AppError>
    suspend fun removeComic(number: Int): Result<Unit, AppError>
}