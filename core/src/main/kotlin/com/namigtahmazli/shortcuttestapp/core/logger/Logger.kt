package com.namigtahmazli.shortcuttestapp.core.logger

import kotlin.coroutines.CoroutineContext

interface Logger {
    fun logError(tag: String, context: CoroutineContext, error: Throwable)
}