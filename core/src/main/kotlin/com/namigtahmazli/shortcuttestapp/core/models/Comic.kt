package com.namigtahmazli.shortcuttestapp.core.models

data class Comic(
    val title: String,
    val image: String,
    val number: Int,
    val date: Date,
    val details: Details,
    val isFavorite: Boolean
) {
    data class Date(
        val day: String,
        val month: String,
        val year: String
    )

    data class Details(
        val transcript: String?,
        val alt: String
    )
}