package com.namigtahmazli.shortcuttestapp.core.mvi

import com.namigtahmazli.shortcuttestapp.core.extensions.exhaustive
import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsStore.State
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsStore.Action
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsStore.Event
import com.namigtahmazli.shortcuttestapp.core.result.*

internal class ComicsExecutor(
    private val dependency: Dependency,
) : Executor<State, Action, Event> {
    override suspend fun executeAction(
        action: Action,
        currentState: () -> State,
        reduce: Reduce<State>,
        dispatch: Dispatch<Event>
    ) {
        when (action) {
            is Action.GetCurrentComic -> getCurrentComic(currentState, reduce)
            is Action.GetComicWithNumber -> getComicWithNumber(action.number, currentState, reduce)
            is Action.GetPrevComic -> getPrevComic(currentState, reduce)
            is Action.GetNextComic -> getNextComic(currentState, reduce)
            is Action.ToggleTooltip -> reduce { currentState().toggleTooltip() }
            is Action.ChangeFavoriteStatusOfCurrentComic -> changeFavoriteStatusOfCurrentComic(
                currentState,
                reduce
            )
            is Action.ShareCurrentComic -> shareCurrentComic(currentState, reduce, dispatch)
        }.exhaustive
    }

    private suspend fun getCurrentComic(
        currentState: () -> State,
        reduce: Reduce<State>
    ) {
        reduce { currentState().showLoading() }
            .then { dependency.getCurrentComic() }
            .map(currentState()::setCurrentComic)
            .getOr(currentState()::showFailed)
            .then { newState -> reduce { newState } }
    }

    private suspend fun getPrevComic(
        currentState: () -> State,
        reduce: Reduce<State>
    ) {
        currentState().currentShownComic
            ?.let { it.number - 1 }
            ?.takeIf { it > 0 }
            ?.then { getComicWithNumber(it, currentState, reduce) }
    }

    private suspend fun getNextComic(
        currentState: () -> State,
        reduce: Reduce<State>
    ) {
        currentState().currentShownComic
            ?.let { it.number + 1 }
            ?.then { getComicWithNumber(it, currentState, reduce) }
    }

    private suspend fun getComicWithNumber(
        number: Int,
        currentState: () -> State,
        reduce: Reduce<State>
    ) {
        reduce { currentState().showLoading() }
            .then { currentState().getComic(number) }
            .getOrFlatMap { dependency.getComicWithNumber(number) }
            .map(currentState()::setCurrentComic)
            .getOr(currentState()::showFailed)
            .then { newState -> reduce { newState } }
    }

    private suspend fun changeFavoriteStatusOfCurrentComic(
        currentState: () -> State,
        reduce: Reduce<State>
    ) {
        currentState().currentShownComic
            ?.then { dependency.changeFavoriteStatusOf(it) }
            ?.map { currentState().updateCurrentComic(it) }
            ?.getOr(currentState)
            ?.then { newState -> reduce { newState } }
    }

    private suspend fun shareCurrentComic(
        currentState: () -> State,
        reduce: Reduce<State>,
        dispatch: Dispatch<Event>
    ) {
        reduce { currentState().showLoading() }
            .then { currentState().currentShownComic }
            ?.then { dependency.getComicImageUri(it) }
            ?.map(Event::Share)
            ?.getOrNull()
            ?.then(dispatch)
            .then { reduce { currentState().hideLoading() } }
    }

    interface Dependency {
        suspend fun getCurrentComic(): Result<Comic, AppError>
        suspend fun getComicWithNumber(number: Int): Result<Comic, AppError>
        suspend fun changeFavoriteStatusOf(comic: Comic): Result<Comic, AppError>
        suspend fun getComicImageUri(comic: Comic): Result<String, AppError>
    }
}