package com.namigtahmazli.shortcuttestapp.core.mvi

import com.namigtahmazli.shortcuttestapp.core.interactors.*
import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import javax.inject.Inject

internal class ComicsExecutorDependency @Inject constructor(
    private val fetchCurrentComic: FetchCurrentComic,
    private val getComicDetails: GetComicDetails,
    private val changeFavoriteStatusOfComic: ChangeFavoriteStatusOfComic,
    private val getComicImageUri: GetComicImageUri
) : ComicsExecutor.Dependency {
    override suspend fun getCurrentComic(): Result<Comic, AppError> {
        return fetchCurrentComic()
    }

    override suspend fun getComicWithNumber(number: Int): Result<Comic, AppError> {
        return getComicDetails(number)
    }

    override suspend fun changeFavoriteStatusOf(comic: Comic): Result<Comic, AppError> {
        return changeFavoriteStatusOfComic(comic)
    }

    override suspend fun getComicImageUri(comic: Comic): Result<String, AppError> {
        return getComicImageUri.invoke(comic)
    }
}