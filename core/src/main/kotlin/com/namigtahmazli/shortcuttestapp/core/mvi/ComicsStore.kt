package com.namigtahmazli.shortcuttestapp.core.mvi

import com.namigtahmazli.shortcuttestapp.core.extensions.replace
import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsStore.State
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsStore.Action
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsStore.Event

interface ComicsStore : Store<State, Action, Event> {
    data class State(
        internal val loadedComics: Set<Comic>,
        val currentShownComic: Comic?,
        val isLoading: Boolean,
        val isFailed: Boolean,
        val isTooltipShown: Boolean
    ) {
        fun showLoading(): State =
            copy(isLoading = true, isFailed = false)

        fun hideLoading(): State = copy(isLoading = false)

        fun showFailed(): State =
            copy(isLoading = false, isFailed = true)

        fun setCurrentComic(comic: Comic): State =
            copy(
                currentShownComic = comic,
                isLoading = false,
                isFailed = false,
                loadedComics = loadedComics + comic
            )

        fun getComic(number: Int): Comic? =
            loadedComics.firstOrNull { it.number == number }

        fun toggleTooltip(): State = copy(isTooltipShown = !isTooltipShown)

        fun updateCurrentComic(comic: Comic): State =
            copy(
                currentShownComic = comic,
                loadedComics = loadedComics.replace(comic) { it.number == comic.number }
            )

        companion object {
            internal fun initial(): State =
                State(
                    loadedComics = emptySet(),
                    currentShownComic = null,
                    isLoading = false,
                    isFailed = false,
                    isTooltipShown = false
                )
        }
    }

    sealed interface Action {
        object GetCurrentComic : Action
        object GetNextComic : Action
        object GetPrevComic : Action
        data class GetComicWithNumber(val number: Int) : Action
        object ToggleTooltip : Action
        object ChangeFavoriteStatusOfCurrentComic : Action
        object ShareCurrentComic : Action
    }

    sealed interface Event {
        data class Share(val uri: String) : Event
    }

    companion object {
        internal fun create(dependency: ComicsExecutor.Dependency): ComicsStore =
            object :
                ComicsStore,
                Store<State, Action, Event> by Store.factory(
                    State.initial(),
                    ComicsExecutor(dependency)
                ) {}
    }
}