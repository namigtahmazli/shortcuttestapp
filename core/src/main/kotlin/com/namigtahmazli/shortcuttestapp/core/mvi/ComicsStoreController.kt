package com.namigtahmazli.shortcuttestapp.core.mvi

import com.namigtahmazli.shortcuttestapp.core.logger.Logger
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import kotlin.coroutines.CoroutineContext

interface ComicsStoreController {
    val states: Flow<ComicsStore.State>
    val events: Flow<ComicsStore.Event>
    fun loadNextComic()
    fun loadPreviousComic()
    fun toggleTooltip()
    fun toggleFavorite()
    fun shareComic()
    fun dispose()

    interface Factory {
        fun create(): ComicsStoreController
    }
}

internal class ComicsStoreControllerFactory @Inject constructor(
    private val dependency: ComicsExecutor.Dependency,
    private val logger: Logger
) : ComicsStoreController.Factory {
    override fun create(): ComicsStoreController {
        return ComicsStoreControllerImpl(dependency, logger)
    }
}

private class ComicsStoreControllerImpl(
    dependency: ComicsExecutor.Dependency,
    private val logger: Logger
) : ComicsStoreController {
    private val job: Job = SupervisorJob()
    private val scope = CoroutineScope(job + CoroutineName("Controller"))
    private val store = ComicsStore.create(dependency)
    override val states: Flow<ComicsStore.State> = store.states
    override val events: Flow<ComicsStore.Event> = store.events

    private var activeJob: Job? = null

    private val exceptionHandler = CoroutineExceptionHandler(this::handleExceptions)

    init {
        loadCurrentComic()
    }

    override fun loadNextComic() {
        cancelPreviousAndLaunch {
            store.execute(ComicsStore.Action.GetNextComic)
        }
    }

    override fun loadPreviousComic() {
        cancelPreviousAndLaunch {
            store.execute(ComicsStore.Action.GetPrevComic)
        }
    }

    override fun toggleTooltip() {
        launch {
            store.execute(ComicsStore.Action.ToggleTooltip)
        }
    }

    override fun toggleFavorite() {
        launch {
            store.execute(ComicsStore.Action.ChangeFavoriteStatusOfCurrentComic)
        }
    }

    override fun shareComic() {
        launch {
            store.execute(ComicsStore.Action.ShareCurrentComic)
        }
    }

    override fun dispose() {
        job.cancel()
    }

    private fun loadCurrentComic() {
        cancelPreviousAndLaunch {
            store.execute(ComicsStore.Action.GetCurrentComic)
        }
    }

    private fun cancelPreviousAndLaunch(block: suspend CoroutineScope.() -> Unit) {
        activeJob?.cancel()
        activeJob = launch(block)
    }

    private fun launch(block: suspend CoroutineScope.() -> Unit): Job {
        return scope.launch(exceptionHandler, block = block)
    }

    private fun handleExceptions(context: CoroutineContext, error: Throwable) {
        logger.logError(tag, context, error)
    }

    companion object {
        private val tag = ComicsStoreController::class.java.name
    }
}