package com.namigtahmazli.shortcuttestapp.core.mvi

import kotlinx.coroutines.channels.BufferOverflow
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow

typealias Reduce<State> = ((State) -> State) -> Unit
typealias Dispatch<Event> = (Event) -> Unit

interface Executor<State : Any, Action : Any, Event: Any> {
    suspend fun executeAction(
        action: Action,
        currentState: () -> State,
        reduce: Reduce<State>,
        dispatch: Dispatch<Event>
    )
}

interface Store<State : Any, Action : Any, Event: Any> {
    val states: Flow<State>
    val currentState: State
    val events: Flow<Event>
    suspend fun execute(action: Action)

    companion object {
        fun <State : Any, Action : Any, Event: Any> factory(
            initialState: State,
            executor: Executor<State, Action, Event>
        ): Store<State, Action, Event> =
            object : Store<State, Action, Event> {
                private val stateFlow: MutableStateFlow<State> = MutableStateFlow(initialState)
                private val eventFlow: MutableSharedFlow<Event> = MutableSharedFlow(
                    replay = 0,
                    extraBufferCapacity = 1,
                    onBufferOverflow = BufferOverflow.DROP_OLDEST
                )

                override val states: Flow<State> = stateFlow
                override var currentState: State
                    private set(value) {
                        stateFlow.value = value
                    }
                    get() {
                        return stateFlow.value
                    }

                override val events: Flow<Event> = eventFlow

                override suspend fun execute(action: Action) {
                    executor.executeAction(
                        action = action,
                        currentState = { currentState },
                        reduce = { reduce -> currentState = reduce(currentState) },
                        dispatch = eventFlow::tryEmit
                    )
                }
            }
    }
}