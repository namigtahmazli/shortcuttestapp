package com.namigtahmazli.shortcuttestapp.core.parsers

import com.namigtahmazli.shortcuttestapp.core.dto.ComicDto
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result

interface ComicsJsonParser {
    fun fromJson(json: String): Result<ComicDto, AppError>
    fun toJson(dto: ComicDto): Result<String, AppError>
}