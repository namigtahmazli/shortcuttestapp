package com.namigtahmazli.shortcuttestapp.core.remoteSource

import java.io.File

interface RemoteSource {
    suspend fun fetchCurrentComic(): Response
    suspend fun fetchComicDetails(comicNumber: Int): Response
    suspend fun downloadComicImage(fromUrl: String, to: File): Response

    companion object {
        private const val baseUrl = "https://xkcd.com"
        const val currentComic = "$baseUrl/info.0.json"
        private const val comicWithDetails = "$baseUrl/{num}/info.0.json"

        fun comicWithDetails(number: Int): String {
            return comicWithDetails.replace("{num}", number.toString())
        }
    }
}