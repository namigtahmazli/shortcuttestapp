package com.namigtahmazli.shortcuttestapp.core.remoteSource

sealed interface Response {
    data class Succeed(val json: String) : Response
    data class Error(
        val code: Int,
        val message: String
    ) : Response
}