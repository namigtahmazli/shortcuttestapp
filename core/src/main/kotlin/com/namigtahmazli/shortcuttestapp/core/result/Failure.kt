package com.namigtahmazli.shortcuttestapp.core.result

sealed class AppError(
    override val message: String? = null,
    override val cause: Throwable? = null
) : Throwable() {
    data class Network(
        val responseCode: Int,
        override val message: String?,
    ) : AppError(message)

    data class JsonSerialization(
        override val cause: Throwable?
    ) : AppError(cause = cause)

    object NoComicFoundInLocalSource : AppError()

    object FileNotFound : AppError()

    data class DatabaseError(
        override val cause: Throwable?
    ) : AppError(cause = cause)

    data class FileIOError(
        override val cause: Throwable?
    ) : AppError(cause = cause)
}