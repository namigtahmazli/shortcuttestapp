package com.namigtahmazli.shortcuttestapp.core.result

import com.namigtahmazli.shortcuttestapp.core.remoteSource.Response

sealed interface Result<out Value : Any, out Error : Throwable> {
    operator fun component1(): Value?
    operator fun component2(): Error?

    class Ok<Value : Any>(val value: Value) : Result<Value, Nothing> {
        override fun component1(): Value {
            return value
        }

        override fun component2(): Nothing? {
            return null
        }

        override fun equals(other: Any?): Boolean {
            if (other == null) return false
            if (other !is Ok<*>) return false
            if (other === this) return true
            return other.value == value
        }

        override fun hashCode(): Int {
            return value.hashCode() + 33
        }

        override fun toString(): String {
            return "Ok($value)"
        }
    }

    class Err<Value : Throwable>(val value: Value) : Result<Nothing, Value> {
        override fun component1(): Nothing? {
            return null
        }

        override fun component2(): Value {
            return value
        }

        override fun equals(other: Any?): Boolean {
            if (other == null) return false
            if (other !is Err<*>) return false
            if (other === this) return true
            return other.value == value
        }

        override fun hashCode(): Int {
            return value.hashCode() + 33
        }

        override fun toString(): String {
            return "Err($value)"
        }
    }
}

inline fun <T1 : Any, T2 : Any, E : AppError> Result<T1, E>.flatMap(
    f: (T1) -> Result<T2, E>
): Result<T2, E> {
    return when (this) {
        is Result.Ok -> f(value)
        is Result.Err -> this
    }
}

inline fun <T1 : Any, T2 : Any, E : AppError> Result<T1, E>.map(
    f: (T1) -> T2
): Result<T2, E> = when (this) {
    is Result.Ok -> Result.Ok(f(value))
    is Result.Err -> this
}

inline fun <T : Any, E : AppError> Result<T, E>.getOr(f: () -> T): T =
    when (this) {
        is Result.Ok -> value
        else -> f()
    }

fun <T : Any, E : AppError> Result<T, E>.getOrNull(): T? =
    when (this) {
        is Result.Ok -> value
        else -> null
    }

inline fun <T1 : Any?, T2 : Any?> T1.then(f: (T1) -> T2): T2 = f(this)

inline fun <T : Any, E : AppError> T?.getOrFlatMap(f: () -> Result<T, E>): Result<T, E> =
    if (this == null) f() else Result.Ok(this)

inline fun <T : Any, E : AppError> Result<T, E>.catchFlatMap(f: () -> Result<T, E>): Result<T, E> =
    when (this) {
        is Result.Ok -> this
        is Result.Err -> f()
    }

inline fun <T : Any, E : AppError> Result<T, E>.catchMap(f: () -> T): Result<T, E> =
    when (this) {
        is Result.Ok -> this
        is Result.Err -> Result.Ok(f())
    }

inline fun <T : Any, E : AppError> Result<T, E>.catchAndFallback(f: () -> Result<*, E>): Result<T, E> =
    when (this) {
        is Result.Ok -> this
        is Result.Err -> f().flatMap { this }
    }

inline fun <T1 : Any, T2 : Any, E : AppError>
    Result<T1, E>.zipWith(f: (T1) -> Result<T2, E>): Result<Pair<T1, T2>, E> =
    flatMap { t1 ->
        f(t1).flatMap { t2 ->
            Result.Ok(t1 to t2)
        }
    }

fun Response.toResult(): Result<Response.Succeed, AppError> {
    return when (this) {
        is Response.Succeed -> Result.Ok(this)
        is Response.Error -> Result.Err(
            AppError.Network(
                responseCode = this.code,
                message = this.message
            )
        )
    }
}

inline fun <T : Any> runJsonParsing(f: () -> T): Result<T, AppError> {
    return try {
        Result.Ok(f())
    } catch (ex: Exception) {
        Result.Err(AppError.JsonSerialization(ex))
    }
}

inline fun <T : Any> runQueries(f: () -> T): Result<T, AppError> {
    return try {
        Result.Ok(f())
    } catch (ex: Exception) {
        Result.Err(AppError.DatabaseError(ex))
    }
}

inline fun <T : Any> runFileIO(f: () -> T): Result<T, AppError> {
    return try {
        Result.Ok(f())
    } catch (ex: Exception) {
        Result.Err(AppError.FileIOError(ex))
    }
}