package com.namigtahmazli.shortcuttestapp.core

import com.namigtahmazli.shortcuttestapp.core.fileManager.FileManager
import com.namigtahmazli.shortcuttestapp.core.interactors.*
import com.namigtahmazli.shortcuttestapp.core.localSource.LocalSource
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsExecutor
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsStore
import com.namigtahmazli.shortcuttestapp.core.mvi.ComicsStoreTest
import com.namigtahmazli.shortcuttestapp.core.parsers.ComicsJsonParser
import com.namigtahmazli.shortcuttestapp.core.remoteSource.RemoteSource
import dagger.Component
import dagger.Module
import dagger.Provides
import io.mockk.mockk
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Runnable
import javax.inject.Scope
import kotlin.coroutines.CoroutineContext

@Scope
annotation class TestScope

@TestScope
@Component(modules = [TestModule::class])
interface TestComponent {
    @Component.Builder
    interface Builder {
        fun build(): TestComponent
    }

    fun inject(fetchComicDetailsTest: FetchComicDetailsTest)
    fun inject(fetchCurrentComicTest: FetchCurrentComicTest)
    fun inject(comicsStoreTest: ComicsStoreTest)
    fun inject(loadComicFromLocalTest: LoadComicFromLocalTest)
    fun inject(getComicDetailsTest: GetComicDetailsTest)
    fun inject(isComicFavoriteTest: IsComicFavoriteTest)
    fun inject(changeFavoriteStatusOfComicTest: ChangeFavoriteStatusOfComicTest)
    fun inject(downloadAndSaveComicImageTemporaryTest: DownloadAndSaveComicImageTemporaryTest)
    fun inject(getComicImageUriTest: GetComicImageUriTest)
}

class TestDispatcher : CoroutineDispatcher() {
    override fun dispatch(context: CoroutineContext, block: Runnable) {
        block.run()
    }
}

@Module
class TestModule {

    @TestScope
    @Provides
    fun provideRemoteSource(): RemoteSource = mockk()

    @TestScope
    @Provides
    fun provideLocalSource(): LocalSource = mockk()

    @TestScope
    @Provides
    fun provideFileManager(): FileManager = mockk()

    @TestScope
    @Provides
    fun provideDispatcher(): CoroutineDispatcher = TestDispatcher()

    @TestScope
    @Provides
    fun provideJsonParser(): ComicsJsonParser = mockk()

    @TestScope
    @Provides
    fun provideFetchComicDetails(): FetchComicDetails = mockk()

    @TestScope
    @Provides
    fun provideIsComicFavorite(): IsComicFavorite = mockk()

    @TestScope
    @Provides
    fun provideGetComicDetails(): GetComicDetails = mockk()

    @TestScope
    @Provides
    fun provideLoadComicFromLocal(): LoadComicFromLocal = mockk()

    @TestScope
    @Provides
    fun provideDownloadAndSaveComicImageTemporary(): DownloadAndSaveComicImageTemporary =
        mockk()

    @TestScope
    @Provides
    internal fun provideComicsExecutorDependency(): ComicsExecutor.Dependency = mockk()

    @TestScope
    @Provides
    internal fun provideStore(dependency: ComicsExecutor.Dependency): ComicsStore =
        ComicsStore.create(dependency)
}