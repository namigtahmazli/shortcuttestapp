package com.namigtahmazli.shortcuttestapp.core.data

import com.namigtahmazli.shortcuttestapp.core.dto.ComicDto
import com.namigtahmazli.shortcuttestapp.core.models.Comic

object TestData {
    val comicDetailsJson = """
        {"month": "7", "num": 614, "link": "", "year": "2009", "news": "", "safe_title": "Woodpecker", "transcript": "A man with a beret and a woman are standing on a boardwalk, leaning on a handrail.", "alt":"If you don't have an extension cord I can get that too.  Because we're friends!  Right?", "img": "https://imgs.xkcd.com/comics/woodpecker.png", "title": "Woodpecker", "day": "24"}
    """.trimIndent()

    val comicWithDetailsDto = object : ComicDto {
        override val month: String = "7"
        override val num: Int = 614
        override val year: String = "2009"
        override val safeTitle: String = "Woodpecker"
        override val transcript: String =
            "A man with a beret and a woman are standing on a boardwalk, leaning on a handrail."
        override val alt: String =
            "If you don't have an extension cord I can get that too.  Because we're friends!  Right?"
        override val img: String = "https://imgs.xkcd.com/comics/woodpecker.png"
        override val day: String = "24"
    }

    val comicWithDetails = Comic(
        title = "Woodpecker",
        image = "https://imgs.xkcd.com/comics/woodpecker.png",
        number = 614,
        date = Comic.Date(
            day = "24",
            month = "7",
            year = "2009"
        ),
        details = Comic.Details(
            transcript = "A man with a beret and a woman are standing on a boardwalk, leaning on a handrail.",
            alt = "If you don't have an extension cord I can get that too.  Because we're friends!  Right?"
        ),
        isFavorite = false
    )

    val currentComicJson = """
        {"month": "9", "num": 2518, "link": "", "year": "2021", "news": "", "safe_title": "Lumpers and Splitters", "transcript": "", "alt": "Anna Karenina is a happy family lumper and unhappy family splitter.", "img": "https://imgs.xkcd.com/comics/lumpers_and_splitters.png", "title": "Lumpers and Splitters", "day": "20"}
    """.trimIndent()

    val currentComicDto = object : ComicDto {
        override val month: String = "9"
        override val num: Int = 2518
        override val year: String = "2021"
        override val safeTitle: String = "Lumpers and Splitters"
        override val transcript: String = ""
        override val alt: String =
            "Anna Karenina is a happy family lumper and unhappy family splitter."
        override val img: String =
            "https://imgs.xkcd.com/comics/lumpers_and_splitters.png"
        override val day: String = "20"
    }

    val currentComic = Comic(
        title = "Lumpers and Splitters",
        image = "https://imgs.xkcd.com/comics/lumpers_and_splitters.png",
        number = 2518,
        date = Comic.Date(
            day = "20",
            month = "9",
            year = "2021"
        ),
        details = Comic.Details(
            transcript = null,
            alt = "Anna Karenina is a happy family lumper and unhappy family splitter."
        ),
        isFavorite = false
    )
}