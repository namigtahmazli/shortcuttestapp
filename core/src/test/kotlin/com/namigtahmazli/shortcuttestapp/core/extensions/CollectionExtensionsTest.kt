package com.namigtahmazli.shortcuttestapp.core.extensions

import com.namigtahmazli.shortcuttestapp.core.data.TestData
import org.junit.Assert.assertEquals
import org.junit.Test

class CollectionExtensionsTest {

    @Test
    fun `test filterNot`() {
        val set = setOf(
            TestData.comicWithDetails.copy(number = 1),
            TestData.comicWithDetails.copy(number = 2),
            TestData.comicWithDetails.copy(number = 3),
        )

        assertEquals(
            setOf(
                TestData.comicWithDetails.copy(number = 1),
                TestData.comicWithDetails.copy(number = 2),
            ),
            set.filterNot { it.number == 3 }.toSet()
        )
    }

    @Test
    fun `replace function will replace the predicted element in the set`() {
        val set = setOf(
            TestData.comicWithDetails.copy(number = 1),
            TestData.comicWithDetails.copy(number = 2),
            TestData.comicWithDetails.copy(number = 3),
        )

        val newData = TestData.comicWithDetails.copy(number = 3, isFavorite = true)
        val newSet = set.replace(newData) { it.number == newData.number }

        assertEquals(
            setOf(
                TestData.comicWithDetails.copy(number = 1),
                TestData.comicWithDetails.copy(number = 2),
                TestData.comicWithDetails.copy(number = 3, isFavorite = true),
            ),
            newSet
        )
    }
}