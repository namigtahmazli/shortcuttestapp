package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.DaggerTestComponent
import com.namigtahmazli.shortcuttestapp.core.data.TestData
import com.namigtahmazli.shortcuttestapp.core.localSource.LocalSource
import com.namigtahmazli.shortcuttestapp.core.result.Result
import io.mockk.coEvery
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

class ChangeFavoriteStatusOfComicTest {

    @Inject
    internal lateinit var changeFavoriteStatusOfComicImpl: ChangeFavoriteStatusOfComicImpl

    @Inject
    lateinit var isComicFavorite: IsComicFavorite

    @Inject
    lateinit var localSource: LocalSource

    @Before
    fun setUp() {
        DaggerTestComponent.builder().build().inject(this)
    }

    @Test
    fun `if comic is favorite then it will be removed from local source`() {
        runBlocking {
            coEvery { isComicFavorite(any(), any()) } returns Result.Ok(true)
            coEvery { localSource.removeComic(any()) } returns Result.Ok(Unit)

            val result = changeFavoriteStatusOfComicImpl(TestData.comicWithDetails)
            assertEquals(
                Result.Ok(TestData.comicWithDetails.copy(isFavorite = false)),
                result
            )
        }
    }

    @Test
    fun `if comic is not favorite then it will be added to local source`() {
        runBlocking {
            coEvery { isComicFavorite(any(), any()) } returns Result.Ok(false)
            coEvery { localSource.saveComic(any()) } returns Result.Ok(Unit)

            val result = changeFavoriteStatusOfComicImpl(TestData.comicWithDetails)
            assertEquals(
                Result.Ok(TestData.comicWithDetails.copy(isFavorite = true)),
                result
            )
        }
    }
}