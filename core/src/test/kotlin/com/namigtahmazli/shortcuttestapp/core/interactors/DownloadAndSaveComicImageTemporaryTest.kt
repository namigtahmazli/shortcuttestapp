package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.DaggerTestComponent
import com.namigtahmazli.shortcuttestapp.core.data.TestData
import com.namigtahmazli.shortcuttestapp.core.fileManager.FileManager
import com.namigtahmazli.shortcuttestapp.core.remoteSource.RemoteSource
import com.namigtahmazli.shortcuttestapp.core.remoteSource.Response
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import io.mockk.coEvery
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.io.File
import javax.inject.Inject

class DownloadAndSaveComicImageTemporaryTest {
    @Inject
    internal lateinit var useCase: DownloadAndSaveComicImageTemporaryImpl

    @Inject
    lateinit var fileManager: FileManager

    @Inject
    lateinit var remoteSource: RemoteSource

    @Before
    fun setUp() {
        DaggerTestComponent.builder().build().inject(this)
    }

    @Test
    fun `if downloading image fails then created image should be removed`() {
        runBlocking {
            coEvery { fileManager.createTemporaryFile(any()) } returns
                Result.Ok(File(""))

            coEvery { remoteSource.downloadComicImage(any(), any()) } returns
                Response.Error(400, "Bad Request")

            coEvery { fileManager.removeFileWith(any()) } returns Result.Ok(Unit)

            val result = useCase(TestData.comicWithDetails)
            assertEquals(
                Result.Err(AppError.Network(400, "Bad Request")),
                result
            )
        }
    }

    @Test
    fun `if downloading image succeeds then the created image file will be returned`() {
        runBlocking {
            coEvery { fileManager.createTemporaryFile(any()) } returns
                Result.Ok(File("1.jpeg"))
            coEvery { remoteSource.downloadComicImage(any(), any()) } returns
                Response.Succeed("")

            val result = useCase(TestData.comicWithDetails)
            assertEquals(
                Result.Ok(File("1.jpeg")),
                result
            )
        }
    }
}