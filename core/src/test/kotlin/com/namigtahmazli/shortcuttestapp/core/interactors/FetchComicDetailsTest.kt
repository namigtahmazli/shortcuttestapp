package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.DaggerTestComponent
import com.namigtahmazli.shortcuttestapp.core.data.TestData
import com.namigtahmazli.shortcuttestapp.core.parsers.ComicsJsonParser
import com.namigtahmazli.shortcuttestapp.core.remoteSource.RemoteSource
import com.namigtahmazli.shortcuttestapp.core.remoteSource.Response
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import io.mockk.coEvery
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

class FetchComicDetailsTest {

    @Inject
    internal lateinit var fetchComicDetails: FetchComicDetailsImpl

    @Inject
    internal lateinit var remoteSource: RemoteSource

    @Inject
    internal lateinit var comicsJsonParser: ComicsJsonParser

    @Inject
    lateinit var isComicFavorite: IsComicFavorite

    @Before
    fun setUp() {
        DaggerTestComponent.builder().build().inject(this)
    }

    @Test
    fun `network failure will be returned if fetching comic details fail`() {
        runBlocking {
            coEvery { remoteSource.fetchComicDetails(any()) } returns
                Response.Error(404, "Bad Request")

            val result = fetchComicDetails(1)

            assertEquals(
                Result.Err(AppError.Network(404, "Bad Request")), //expected
                result //actual
            )
        }
    }

    @Test
    fun `serialisation failure will be returned if parsing comics fails`() {
        runBlocking {
            val json = TestData.comicDetailsJson
            coEvery { remoteSource.fetchComicDetails(any()) } returns
                Response.Succeed(json)
            coEvery { comicsJsonParser.fromJson(any()) } returns
                Result.Err(AppError.JsonSerialization(null))

            val result = fetchComicDetails(614)

            assertEquals(
                Result.Err(AppError.JsonSerialization(null)), // expected
                result // actual
            )
        }
    }

    @Test
    fun `if fetching and parsing comic details succeed then comics will be returned as model`() {
        runBlocking {
            val json = TestData.comicDetailsJson

            coEvery { remoteSource.fetchComicDetails(any()) } returns
                Response.Succeed(json)

            coEvery { comicsJsonParser.fromJson(any()) } returns
                Result.Ok(TestData.comicWithDetailsDto)

            coEvery { isComicFavorite.invoke(any(), any()) } returns
                Result.Ok(false)

            var result = fetchComicDetails(614)

            assertEquals(
                Result.Ok(TestData.comicWithDetails), // expected
                result // actual
            )

            coEvery { isComicFavorite.invoke(any(), any()) } returns
                Result.Ok(true)

            result = fetchComicDetails(614)

            assertEquals(
                Result.Ok(TestData.comicWithDetails.copy(isFavorite = true)), // expected
                result // actual
            )
        }
    }
}