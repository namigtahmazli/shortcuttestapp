package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.DaggerTestComponent
import com.namigtahmazli.shortcuttestapp.core.data.TestData
import com.namigtahmazli.shortcuttestapp.core.parsers.ComicsJsonParser
import com.namigtahmazli.shortcuttestapp.core.remoteSource.RemoteSource
import com.namigtahmazli.shortcuttestapp.core.remoteSource.Response
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import io.mockk.coEvery
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

class FetchCurrentComicTest {

    @Inject
    internal lateinit var fetchCurrentComic: FetchCurrentComicImpl

    @Inject
    lateinit var getComicDetails: GetComicDetails

    @Inject
    lateinit var remoteSource: RemoteSource

    @Inject
    lateinit var jsonParser: ComicsJsonParser

    @Before
    fun setUp() {
        DaggerTestComponent.builder().build().inject(this)
    }

    @Test
    fun `network error will be returned if fetching current comic fails`() {
        runBlocking {
            coEvery { remoteSource.fetchCurrentComic() } returns
                Response.Error(400, "Bad Request")

            val result = fetchCurrentComic()

            assertEquals(
                Result.Err(AppError.Network(400, "Bad Request")), // expected
                result // actual
            )
        }
    }

    @Test
    fun `if parsing comic fails then serialisation error will be returned`() {
        runBlocking {
            val json = TestData.currentComicJson
            coEvery { remoteSource.fetchCurrentComic() } returns
                Response.Succeed(json)

            coEvery { jsonParser.fromJson(any()) } returns
                Result.Err(AppError.JsonSerialization(null))

            val result = fetchCurrentComic()

            assertEquals(
                Result.Err(AppError.JsonSerialization(null)), // expected
                result // actual
            )
        }
    }

    @Test
    fun `if fetching and parsing current comic succeeds the comic details will be fetched`() {
        runBlocking {
            val json = TestData.currentComicJson

            coEvery { remoteSource.fetchCurrentComic() } returns
                Response.Succeed(json)

            coEvery { jsonParser.fromJson(any()) } returns
                Result.Ok(TestData.currentComicDto)

            val expectedComicModel = TestData.currentComic

            coEvery { getComicDetails.invoke(any(), any()) } returns
                Result.Ok(expectedComicModel)

            var result = fetchCurrentComic()

            assertEquals(
                Result.Ok(expectedComicModel), // expected
                result // actual
            )

            coEvery { getComicDetails.invoke(any(), any()) } returns
                Result.Err(AppError.JsonSerialization(null))

            result = fetchCurrentComic()

            assertEquals(
                Result.Err(AppError.JsonSerialization(null)), // expected
                result // actual
            )
        }
    }
}