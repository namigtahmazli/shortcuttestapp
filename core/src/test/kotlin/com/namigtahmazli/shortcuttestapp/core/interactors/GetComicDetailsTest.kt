package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.DaggerTestComponent
import com.namigtahmazli.shortcuttestapp.core.data.TestData
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import io.mockk.coEvery
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

class GetComicDetailsTest {

    @Inject
    internal lateinit var getComicDetailsImpl: GetComicDetailsImpl

    @Inject
    lateinit var fetchComicDetails: FetchComicDetails

    @Inject
    lateinit var loadComicFromLocal: LoadComicFromLocal

    @Before
    fun setUp() {
        DaggerTestComponent.builder().build().inject(this)
    }

    @Test
    fun `if comic is available in local source no network call will be made`() {
        runBlocking {
            coEvery { loadComicFromLocal(any(), any()) } returns
                Result.Ok(TestData.comicWithDetails.copy(isFavorite = true))

            val result = getComicDetailsImpl(1)
            assertEquals(
                Result.Ok(TestData.comicWithDetails.copy(isFavorite = true)),
                result
            )
        }
    }

    @Test
    fun `if comic is not available in local source then it will be fetched from network`() {
        runBlocking {
            coEvery { loadComicFromLocal(any(), any()) } returns
                Result.Err(AppError.NoComicFoundInLocalSource)

            coEvery { fetchComicDetails(any(), any()) } returns
                Result.Ok(TestData.comicWithDetails)

            val result = getComicDetailsImpl(1)
            assertEquals(
                Result.Ok(TestData.comicWithDetails),
                result
            )
        }
    }
}