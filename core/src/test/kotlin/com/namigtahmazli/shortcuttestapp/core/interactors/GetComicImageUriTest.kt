package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.DaggerTestComponent
import com.namigtahmazli.shortcuttestapp.core.data.TestData
import com.namigtahmazli.shortcuttestapp.core.fileManager.FileManager
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import io.mockk.coEvery
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import java.io.File
import javax.inject.Inject

class GetComicImageUriTest {
    @Inject
    internal lateinit var useCase: GetComicImageUriImpl

    @Inject
    lateinit var fileManager: FileManager

    @Inject
    lateinit var downloadAndSaveComicImageTemporary: DownloadAndSaveComicImageTemporary

    @Before
    fun setUp() {
        DaggerTestComponent.builder().build().inject(this)
    }

    @Test
    fun `if file is already cached then no need to download it again`() {
        runBlocking {
            coEvery { fileManager.getFile(any()) } returns
                Result.Ok(File("1.jpeg"))

            coEvery { fileManager.createUriForFile(any()) } returns
                Result.Ok("content://1.jpeg")

            val result = useCase(TestData.currentComic)
            assertEquals(
                Result.Ok("content://1.jpeg"),
                result
            )
        }
    }

    @Test
    fun `if file is not cached before then it will be fetched and cached`() {
        runBlocking {
            coEvery { fileManager.getFile(any()) } returns
                Result.Err(AppError.FileNotFound)

            coEvery { downloadAndSaveComicImageTemporary(any(), any()) } returns
                Result.Ok(File("1.jpeg"))

            coEvery { fileManager.createUriForFile(any()) } returns
                Result.Ok("content://1.jpeg")

            val result = useCase(TestData.comicWithDetails)
            assertEquals(
                Result.Ok("content://1.jpeg"),
                result
            )
        }
    }
}