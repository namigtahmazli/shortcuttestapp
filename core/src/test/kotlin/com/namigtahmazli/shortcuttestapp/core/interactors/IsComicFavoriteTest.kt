package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.DaggerTestComponent
import com.namigtahmazli.shortcuttestapp.core.data.TestData
import com.namigtahmazli.shortcuttestapp.core.localSource.LocalSource
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import io.mockk.coEvery
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

class IsComicFavoriteTest {
    @Inject
    internal lateinit var isComicFavorite: IsComicFavoriteImpl

    @Inject
    lateinit var localSource: LocalSource

    @Before
    fun setUp() {
        DaggerTestComponent.builder().build().inject(this)
    }

    @Test
    fun `if comic is available in local source then it is favorite otherwise not`() {
        runBlocking {
            coEvery { localSource.retrieveComic(any()) } returns
                Result.Err(AppError.NoComicFoundInLocalSource)

            var result = isComicFavorite(1)
            assertEquals(
                Result.Ok(false),
                result
            )

            coEvery { localSource.retrieveComic(any()) } returns
                Result.Ok(TestData.comicWithDetailsDto)

            result = isComicFavorite(1)
            assertEquals(
                Result.Ok(true),
                result
            )
        }
    }
}