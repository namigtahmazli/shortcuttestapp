package com.namigtahmazli.shortcuttestapp.core.interactors

import com.namigtahmazli.shortcuttestapp.core.DaggerTestComponent
import com.namigtahmazli.shortcuttestapp.core.data.TestData
import com.namigtahmazli.shortcuttestapp.core.localSource.LocalSource
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import io.mockk.coEvery
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import javax.inject.Inject

class LoadComicFromLocalTest {

    @Inject
    internal lateinit var loadComicFromLocalImpl: LoadComicFromLocalImpl

    @Inject
    lateinit var localSource: LocalSource

    @Before
    fun setUp() {
        DaggerTestComponent.builder().build().inject(this)
    }

    @Test
    fun `error will be returned if no comic found in local source`() {
        runBlocking {
            coEvery { localSource.retrieveComic(any()) } returns
                Result.Err(AppError.NoComicFoundInLocalSource)

            val result = loadComicFromLocalImpl(1)
            assertEquals(
                Result.Err(AppError.NoComicFoundInLocalSource), //expected,
                result //actual
            )
        }
    }

    @Test
    fun `comic will be returned if it is available in local source`() {
        runBlocking {
            coEvery { localSource.retrieveComic(any()) } returns
                Result.Ok(TestData.currentComicDto)

            val result = loadComicFromLocalImpl(1)
            assertEquals(
                Result.Ok(TestData.currentComic.copy(isFavorite = true)),
                result
            )
        }
    }
}