package com.namigtahmazli.shortcuttestapp.core.mvi

import app.cash.turbine.test
import com.namigtahmazli.shortcuttestapp.core.DaggerTestComponent
import com.namigtahmazli.shortcuttestapp.core.data.TestData
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import io.mockk.coEvery
import kotlinx.coroutines.runBlocking
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import javax.inject.Inject
import kotlin.time.ExperimentalTime

@OptIn(ExperimentalTime::class)
class ComicsStoreTest {

    @Inject
    internal lateinit var dependency: ComicsExecutor.Dependency

    @Inject
    lateinit var store: ComicsStore

    @Before
    fun setUp() {
        DaggerTestComponent.builder().build().inject(this)
    }

    @Test
    fun `the current state equals to initial state`() {
        runBlocking {
            assertEquals(
                ComicsStore.State.initial(), // expected
                store.currentState // actual
            )
        }
    }

    @Test
    fun `correct states will be set if fetching current comics succeed`() {
        runBlocking {
            coEvery { dependency.getCurrentComic() } returns
                Result.Ok(TestData.comicWithDetails)

            store.states.test {
                assertEquals(ComicsStore.State.initial(), awaitItem())
                store.execute(ComicsStore.Action.GetCurrentComic)

                assertEquals(ComicsStore.State.initial().showLoading(), awaitItem())
                assertEquals(
                    ComicsStore.State.initial().setCurrentComic(TestData.comicWithDetails),
                    awaitItem()
                )
            }
        }
    }

    @Test
    fun `correct states will be set if fetching current comics fails`() {
        runBlocking {
            coEvery { dependency.getCurrentComic() } returns
                Result.Err(AppError.Network(400, "Bad Request"))

            store.states.test {
                assertEquals(ComicsStore.State.initial(), awaitItem())
                store.execute(ComicsStore.Action.GetCurrentComic)
                assertEquals(ComicsStore.State.initial().showLoading(), awaitItem())
                assertEquals(ComicsStore.State.initial().showFailed(), awaitItem())
            }
        }
    }

    @Test
    fun `correct states will be set if fetching comic details succeed`() {
        runBlocking {
            coEvery { dependency.getComicWithNumber(any()) } returns
                Result.Ok(TestData.comicWithDetails)

            store.states.test {
                assertEquals(ComicsStore.State.initial(), awaitItem())
                store.execute(ComicsStore.Action.GetComicWithNumber(1))
                assertEquals(ComicsStore.State.initial().showLoading(), awaitItem())
                assertEquals(
                    ComicsStore.State.initial().setCurrentComic(TestData.comicWithDetails),
                    awaitItem()
                )
            }
        }
    }

    @Test
    fun `correct states will be set if fetching comic details fails`() {
        runBlocking {
            coEvery { dependency.getComicWithNumber(any()) } returns
                Result.Err(AppError.JsonSerialization(null))
            store.states.test {
                assertEquals(ComicsStore.State.initial(), awaitItem())
                store.execute(ComicsStore.Action.GetComicWithNumber(1))
                assertEquals(ComicsStore.State.initial().showLoading(), awaitItem())
                assertEquals(ComicsStore.State.initial().showFailed(), awaitItem())
            }
        }
    }

    @Test
    fun `toggling show tooltip will update the state`() {
        runBlocking {
            store.states.test {
                assertEquals(ComicsStore.State.initial(), awaitItem())
                store.execute(ComicsStore.Action.ToggleTooltip)
                assertEquals(ComicsStore.State.initial().toggleTooltip(), awaitItem())
                store.execute(ComicsStore.Action.ToggleTooltip)
                assertEquals(
                    ComicsStore.State.initial()
                        .toggleTooltip()
                        .toggleTooltip(),
                    awaitItem()
                )
            }
        }
    }

    @Test
    fun `toggling favorite will change current comic and update the state`() {
        runBlocking {
            store.states.test {
                coEvery { dependency.changeFavoriteStatusOf(any()) } returns
                    Result.Ok(TestData.comicWithDetails.copy(isFavorite = true))

                coEvery { dependency.getCurrentComic() } returns
                    Result.Ok(TestData.comicWithDetails.copy(isFavorite = false))

                assertEquals(ComicsStore.State.initial(), awaitItem())
                store.execute(ComicsStore.Action.GetCurrentComic)
                assertEquals(ComicsStore.State.initial().showLoading(), awaitItem())
                assertEquals(
                    ComicsStore.State.initial()
                        .setCurrentComic(TestData.comicWithDetails.copy(isFavorite = false)),
                    awaitItem()
                )

                store.execute(ComicsStore.Action.ChangeFavoriteStatusOfCurrentComic)
                assertEquals(
                    ComicsStore.State.initial()
                        .setCurrentComic(TestData.comicWithDetails.copy(isFavorite = false))
                        .updateCurrentComic(TestData.comicWithDetails.copy(isFavorite = true)),
                    awaitItem()
                )
            }
        }
    }

    @Test
    fun `triggering share action will get image uri and dispatch it back as Event`() {
        runBlocking {
            store.events.test event@{
                store.states.test {
                    coEvery { dependency.getCurrentComic() } returns
                        Result.Ok(TestData.currentComic)
                    coEvery { dependency.getComicImageUri(any()) } returns
                        Result.Ok("content://1.jpeg")

                    assertEquals(ComicsStore.State.initial(), awaitItem())
                    store.execute(ComicsStore.Action.GetCurrentComic)
                    assertEquals(
                        ComicsStore.State.initial()
                            .showLoading(), awaitItem()
                    )
                    assertEquals(
                        ComicsStore.State.initial()
                            .setCurrentComic(TestData.currentComic), awaitItem()
                    )
                    store.execute(ComicsStore.Action.ShareCurrentComic)
                    assertEquals(
                        ComicsStore.State.initial()
                            .setCurrentComic(TestData.currentComic)
                            .showLoading(), awaitItem()
                    )

                    assertEquals(
                        ComicsStore.Event.Share("content://1.jpeg"),
                        this@event.awaitItem()
                    )

                    assertEquals(
                        ComicsStore.State.initial()
                            .setCurrentComic(TestData.currentComic)
                            .hideLoading(), awaitItem()
                    )
                }
            }
        }
    }
}