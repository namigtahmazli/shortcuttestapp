package com.namigtahmazli.shortcuttestapp.fileManager

import android.content.Context
import androidx.core.content.FileProvider
import com.namigtahmazli.shortcuttestapp.core.fileManager.FileManager
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import com.namigtahmazli.shortcuttestapp.core.result.runFileIO
import java.io.File
import javax.inject.Inject

internal class FileManagerImpl @Inject constructor(
    private val context: Context
) : FileManager {
    override fun getFile(name: String): Result<File, AppError> {
        return runFileIO {
            File(context.cacheDir, name)
                .takeIf { it.exists() }
                ?: return Result.Err(AppError.FileNotFound)
        }
    }

    override fun createTemporaryFile(name: String): Result<File, AppError> {
        return runFileIO {
            File(context.cacheDir, name)
                .also { if (!it.exists()) it.createNewFile() }
        }
    }

    override fun removeFileWith(name: String): Result<Unit, AppError> {
        return runFileIO {
            File(context.cacheDir, name)
                .also { if (it.exists()) it.delete() }
        }
    }

    override fun createUriForFile(name: String): Result<String, AppError> {
        return runFileIO {
            FileProvider.getUriForFile(
                context,
                context.getString(R.string.file_provider_auth),
                File(context.cacheDir, name)
            ).toString()
        }
    }
}