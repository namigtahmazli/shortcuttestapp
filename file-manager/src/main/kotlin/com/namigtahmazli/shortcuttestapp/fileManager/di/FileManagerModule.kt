package com.namigtahmazli.shortcuttestapp.fileManager.di

import com.namigtahmazli.shortcuttestapp.core.di.AppScope
import com.namigtahmazli.shortcuttestapp.core.fileManager.FileManager
import com.namigtahmazli.shortcuttestapp.fileManager.FileManagerImpl
import dagger.Binds
import dagger.Module

@Module
abstract class FileManagerModule {
    @Binds
    @AppScope
    internal abstract fun bind(impl: FileManagerImpl): FileManager
}