package com.namigtahmazli.shortcuttestapp.jsonParser

import com.namigtahmazli.shortcuttestapp.core.dto.ComicDto
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable

@Serializable
internal data class ComicsDtoImpl(
    override val month: String,
    override val num: Int,
    override val year: String,
    @SerialName("safe_title") override val safeTitle: String,
    override val transcript: String,
    override val alt: String,
    override val img: String,
    override val day: String
) : ComicDto {
    companion object {
        fun from(other: ComicDto) : ComicsDtoImpl {
            return ComicsDtoImpl(
                month = other.month,
                num = other.num,
                year = other.year,
                safeTitle = other.safeTitle,
                transcript = other.transcript,
                alt = other.alt,
                img = other.img,
                day = other.day
            )
        }
    }
}