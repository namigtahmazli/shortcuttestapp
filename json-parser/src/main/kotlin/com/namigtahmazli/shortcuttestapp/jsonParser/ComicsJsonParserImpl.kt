package com.namigtahmazli.shortcuttestapp.jsonParser

import com.namigtahmazli.shortcuttestapp.core.parsers.ComicsJsonParser
import com.namigtahmazli.shortcuttestapp.core.dto.ComicDto
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import com.namigtahmazli.shortcuttestapp.core.result.runJsonParsing
import kotlinx.serialization.json.Json
import javax.inject.Inject

internal class ComicsJsonParserImpl @Inject constructor() : ComicsJsonParser {
    private val jsonParser = Json {
        prettyPrint = true
        ignoreUnknownKeys = true
    }

    override fun fromJson(json: String): Result<ComicDto, AppError> {
        return runJsonParsing {
            jsonParser.decodeFromString(ComicsDtoImpl.serializer(), json)
        }
    }

    override fun toJson(dto: ComicDto): Result<String, AppError> {
        return runJsonParsing {
            jsonParser.encodeToString(ComicsDtoImpl.serializer(), dto as ComicsDtoImpl)
        }
    }
}
