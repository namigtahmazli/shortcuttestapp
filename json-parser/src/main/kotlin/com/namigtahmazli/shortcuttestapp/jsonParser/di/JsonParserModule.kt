package com.namigtahmazli.shortcuttestapp.jsonParser.di

import com.namigtahmazli.shortcuttestapp.core.parsers.ComicsJsonParser
import com.namigtahmazli.shortcuttestapp.core.di.AppScope
import com.namigtahmazli.shortcuttestapp.jsonParser.ComicsJsonParserImpl
import dagger.Binds
import dagger.Module

@Module
abstract class JsonParserModule {
    @Binds
    @AppScope
    internal abstract fun bind(impl: ComicsJsonParserImpl): ComicsJsonParser
}