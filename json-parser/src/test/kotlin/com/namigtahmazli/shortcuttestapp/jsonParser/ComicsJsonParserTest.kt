package com.namigtahmazli.shortcuttestapp.jsonParser

import com.namigtahmazli.shortcuttestapp.core.data.TestData
import com.namigtahmazli.shortcuttestapp.core.result.Result
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test

class ComicsJsonParserTest {

    private lateinit var jsonParser: ComicsJsonParserImpl

    @Before
    fun setUp() {
        jsonParser = ComicsJsonParserImpl()
    }

    @Test
    fun `parsing comic json will succeed`() {
        val json = TestData.comicDetailsJson
        val result = jsonParser.fromJson(json)
        println(result)
        assertEquals(
            Result.Ok(ComicsDtoImpl.from(TestData.comicWithDetailsDto)),
            result
        )
    }
}