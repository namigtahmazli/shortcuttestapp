package com.namigtahmazli.shortcuttestapp.localSource

import com.namigtahmazli.shortcuttestapp.core.dto.ComicDto
import com.namigtahmazli.shortcuttestapp.core.localSource.LocalSource
import com.namigtahmazli.shortcuttestapp.core.models.Comic
import com.namigtahmazli.shortcuttestapp.core.result.AppError
import com.namigtahmazli.shortcuttestapp.core.result.Result
import com.namigtahmazli.shortcuttestapp.core.result.runQueries
import tables.ComicsQueries
import javax.inject.Inject

internal class LocalSourceImpl @Inject constructor(
    private val queries: ComicsQueries
) : LocalSource {

    override suspend fun saveComic(comic: Comic): Result<Unit, AppError> {
        return runQueries {
            with(comic) {
                queries.insert(
                    month = date.month,
                    num = number.toLong(),
                    year = date.year,
                    transcript = details.transcript ?: "",
                    safeTitle = title,
                    alt = details.alt,
                    img = image,
                    day = date.day
                )
            }
        }
    }

    override suspend fun retrieveComic(number: Int): Result<ComicDto, AppError> {
        return runQueries {
            queries.selectByNumber(number.toLong()).executeAsOneOrNull()
                ?.let {
                    object : ComicDto {
                        override val month: String = it.month
                        override val num: Int = it.num.toInt()
                        override val year: String = it.year
                        override val safeTitle: String = it.safeTitle
                        override val transcript: String = it.transcript
                        override val alt: String = it.alt
                        override val img: String = it.img
                        override val day: String = it.day
                    }
                }
                ?: return Result.Err(AppError.NoComicFoundInLocalSource)
        }
    }

    override suspend fun removeComic(number: Int): Result<Unit, AppError> {
        return runQueries {
            queries.delete(number.toLong())
        }
    }
}