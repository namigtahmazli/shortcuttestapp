package com.namigtahmazli.shortcuttestapp.localSource

import android.content.Context
import com.namigtahmazli.shortcuttestapp.core.di.AppScope
import com.namigtahmazli.shortcuttestapp.core.localSource.LocalSource
import com.squareup.sqldelight.android.AndroidSqliteDriver
import com.squareup.sqldelight.db.SqlDriver
import dagger.Binds
import dagger.Module
import dagger.Provides
import tables.ComicsQueries

@Module
abstract class LocalSourceModule {
    @Binds
    internal abstract fun bindLocalSource(impl: LocalSourceImpl): LocalSource

    companion object {
        @AppScope
        @Provides
        fun provideDatabase(driver: SqlDriver): ShortcutTestAppDB =
            ShortcutTestAppDB.invoke(driver)

        @Provides
        fun provideComicsQueries(db: ShortcutTestAppDB): ComicsQueries =
            db.comicsQueries


        @AppScope
        @Provides
        fun provideDriver(context: Context): SqlDriver =
            AndroidSqliteDriver(
                ShortcutTestAppDB.Schema, context,
                name = "ShortcutTestApp.db"
            )
    }
}