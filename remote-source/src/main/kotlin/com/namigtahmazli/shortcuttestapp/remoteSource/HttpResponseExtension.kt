package com.namigtahmazli.shortcuttestapp.remoteSource

import com.namigtahmazli.shortcuttestapp.core.remoteSource.Response
import io.ktor.client.statement.*

suspend fun HttpResponse.asResponse(): Response {
    val status = this.status.value
    val content = this.readText()
    return if (status == 200) {
        Response.Succeed(content)
    } else {
        Response.Error(
            code = status,
            message = content
        )
    }
}