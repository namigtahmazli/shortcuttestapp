package com.namigtahmazli.shortcuttestapp.remoteSource

import com.namigtahmazli.shortcuttestapp.core.remoteSource.RemoteSource
import com.namigtahmazli.shortcuttestapp.core.remoteSource.Response
import com.namigtahmazli.shortcuttestapp.core.result.then
import io.ktor.client.*
import io.ktor.client.request.*
import io.ktor.client.statement.*
import io.ktor.http.*
import io.ktor.util.cio.*
import io.ktor.utils.io.*
import java.io.File
import javax.inject.Inject

internal class RemoteSourceImpl @Inject constructor(
    private val client: HttpClient
) : RemoteSource {
    override suspend fun fetchCurrentComic(): Response {
        return makeRequest { RemoteSource.currentComic }
    }

    override suspend fun fetchComicDetails(comicNumber: Int): Response {
        return makeRequest { RemoteSource.comicWithDetails(comicNumber) }
    }

    override suspend fun downloadComicImage(fromUrl: String, to: File): Response {
        return client.request<HttpResponse> {
            method = HttpMethod.Get
            url(fromUrl)
        }.then { response ->
            if (response.status.isSuccess()) {
                response.content.copyAndClose(to.writeChannel())
                    .let { Response.Succeed("") }
            } else {
                Response.Error(response.status.value, response.readText())
            }
        }
    }

    private suspend fun makeRequest(url: () -> String): Response {
        return client.request<HttpResponse> {
            url(url())
            method = HttpMethod.Get
            headers {
                append(HttpHeaders.Accept, "application/json")
                append(HttpHeaders.Connection, "close")
            }
        }.asResponse()
    }
}