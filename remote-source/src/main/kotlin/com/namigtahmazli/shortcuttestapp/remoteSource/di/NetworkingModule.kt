package com.namigtahmazli.shortcuttestapp.remoteSource.di

import android.util.Log
import com.namigtahmazli.shortcuttestapp.core.di.AppScope
import dagger.Module
import dagger.Provides
import io.ktor.client.*
import io.ktor.client.engine.android.*
import io.ktor.client.features.logging.*

@Module
class NetworkingModule {
    @AppScope
    @Provides
    fun provideClient() : HttpClient =
        HttpClient(Android) {
            install(Logging) {
                logger = object : Logger {
                    override fun log(message: String) {
                        Log.d(TAG, message)
                    }
                }
            }
        }

    companion object {
        const val TAG = "Network"
    }
}