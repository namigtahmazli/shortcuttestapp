package com.namigtahmazli.shortcuttestapp.remoteSource.di

import com.namigtahmazli.shortcuttestapp.core.di.AppScope
import com.namigtahmazli.shortcuttestapp.core.remoteSource.RemoteSource
import com.namigtahmazli.shortcuttestapp.remoteSource.RemoteSourceImpl
import dagger.Binds
import dagger.Module

@Module(includes = [NetworkingModule::class])
abstract class RemoteSourceModule {
    @Binds
    @AppScope
    internal abstract fun bind(impl: RemoteSourceImpl): RemoteSource
}